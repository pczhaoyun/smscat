from django.conf.urls import patterns, include, url
from django.contrib import admin
import gateway


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('gateway.urls')),
)
