#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Django settings for smscat project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from local_settings import *
import os
from kombu import Exchange, Queue
from datetime import timedelta
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'l=i2ir8#j)-^!ct(_l2ei_y-%^^epmleamt=g%!))mcne_^*26'

TEMPLATE_DEBUG = DEBUG


# Application definition

INSTALLED_APPS = (
    # 'material',
    # 'material.frontend',
    # 'material.admin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'gateway',
    'smscat',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'smscat.urls'

WSGI_APPLICATION = 'smscat.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'zh-hans'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATICFILES_FINDERS = (
    # 'compressor.finders.CompressorFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

STATIC_URL = '/static/'

if DEBUG:
    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )
else:
    TEMPLATE_LOADERS = (
        ('django.template.loaders.cached.Loader', (
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        )),
    )

# ===============================================================================
# 任务队列
# ===============================================================================
"""
http://stackoverflow.com/questions/16200532/running-multiple-instances-of-celery-on-the-same-server

I solved this by using a virtual host for celery.

Once the rabbitmq server is running I issue these commands:

rabbitmqctl add_user user password
rabbitmqctl add_vhost app2
rabbitmqctl set_permissions -p app2 user ".*" ".*" ".*"
Then I start celery with:

celery -A tasks worker --broker=amqp://user:password@localhost/app2
With my task, I initialize the celery object like this:

celery = Celery('tasks', backend='amqp', broker='amqp://user:password@localhost:5672/app2
"""

BROKER_URL = 'amqp://smscat:smscat@localhost:5672/smscat'
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'

CELERYBEAT_SCHEDULE = {
    'add-every-2-seconds': {
        'task': 'gateway.tasks.sms_engine',
        'schedule': timedelta(seconds=2),
    },
}

CELERY_QUEUES = (
    Queue('smscat', Exchange('smscat'), routing_key='smscat'),
)

CELERY_DEFAULT_QUEUE = 'smscat'

CELERY_DEFAULT_EXCHANGE = 'smscat'

CELERY_DEFAULT_ROUTING_KEY = 'smscat'

CELERY_ROUTES = {
    'gateway.tasks.sms_handler': {'queue': 'smscat'},
    'gateway.tasks.sms_engine': {'queue': 'smscat'},
}
