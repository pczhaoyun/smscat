#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from requests import RequestException
from base import BaseBackend


class ChuangLanBackend(BaseBackend):
    """ 创蓝短信发送通道 """

    name = 'chuanglan'
    display_name = u"创蓝"
    description = u"创蓝短信发送通道"

    address = "http://222.73.117.158:80/msg/HttpBatchSendSM"

    def send_message(self, mobile, message):
        """ 发送消息Http接口
        :return bool: 返回调用远程短信接口是否成功
        """

        tpl = {'account': 'fuerdai',
               'pswd': 'Tch123456',
               'mobile': mobile,
               'msg': message,
               'needstatus': 'true',
               }

        """
        resptime,respstatus
        msgid
        短信提交响应分为两行，第一行为响应时间和状态，第二行为服务器给出提交msgid。
        无论发送的号码是多少，一个发送请求只返回一个msgid，如果响应的状态不是“0”，则没有msgid即第二行数据。（每行以换行符(0x0a,即\n)分割）
        """

        try:
            response = self.get(self.address, params=tpl)
        except RequestException:
            return {'status': 'fail', 'detail': 'request fail', 'retry': True}
        else:
            lines = [s for s in response.content.split() if s.strip()]

            resptime, respstatus = lines[0].split(',')
            msgid = ''

            if len(lines) == 2:
                msgid = lines[1]

            if int(respstatus) == 0:
                return {'status': 'success', 'msgid': msgid, 'retry': False}
            else:
                return {'status': 'fail', 'detail': respstatus, 'retry': False}

    def send_verify_code(self, mobile, verify_code):
        """ 发送验证码
        :return bool: 返回调用远程短信接口是否成功
        """

        msg = u'{0}（付尓代手机验证码，请完成验证），如非本人操作，请忽略本短信'.format(verify_code)
        return self.send_message(mobile, msg)

    def parse_delivery_status(self, data):
        gateway_message_id = str(data['msgid'])
        state = str(data['status'])
        mobile = str(data['mobile'])
        delivery_date = datetime.strptime(str(data['reportTime']), '%y%m%d%H%M%S')

        detail = state
        if state == 'DELIVRD':
            state = 'Delivered'
        else:
            state = 'Failed'

        return [{'gateway_message_id': gateway_message_id,
                 'state': state,
                 'mobile': mobile,
                 'delivery_date': delivery_date,
                 'detail': detail,
                 }
                ]

if __name__ == '__main__':
    backend = ChuangLanBackend()
    print backend.send_message(mobile='15067145712', message=u'尊敬的分享付店小二您好！今日分享付平台对店小二补贴是每笔订单的10%哦。赶紧向客人推荐用分享付买单吧！祝你多多接单、多拿补贴！下载地址http://www.baidu.com/')
