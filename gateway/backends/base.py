#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests


class BaseBackend(object):
    """
    Base class for sms backend implementations.

    Subclasses must at least overwrite send_messages()
    """

    name = None
    display_name = None
    description = None

    def __init__(self, fail_silently=False, **kwargs):
        self.fail_silently = fail_silently

    def open(self):
        """
        Open a network connection.

        This method can be overwritten by backend implementations to open a network connection.
        It's up to the backend implementation to track the status of
        a network connection if it's needed by the backend.
        This method can be called by applications to force a single
        network connection to be used when sending multiple SMSs.

        The default implementation does nothing.
        """
        pass

    def close(self):
        """Close a network connection"""
        pass

    def get(self, url, **kwargs):
        """Wrapper get request method"""
        return requests.get(url, **kwargs)

    def post(self, url, data=None, json=None, **kwargs):
        """Wrapper post request method"""
        return requests.post(url, data, json, **kwargs)

    def send_message(self, mobile, message):
        """
        Sends one or more SmsMessage objects and returns the number of messages sent
        """
        raise NotImplementedError

    def return_delivery_status(self):
        return 'OK'

    def parse_delivery_status(self, data):
        """ 解析数据 """
        raise NotImplementedError
