#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import urllib
from datetime import datetime
from requests import RequestException
from base import BaseBackend


class YunPianBackend(BaseBackend):
    """ 云片短信发送通道 """

    name = 'yunpian'
    display_name = u"云片"
    description = u"云片短信发送通道"
    address = "http://yunpian.com/v1/sms/send.json"

    def send_message(self, mobile, message):
        """ 发送消息Http接口
        :return bool: 返回调用远程短信接口是否成功
        """

        message = u"【付尔代网络科技】" + message

        tpl = {'apikey': '6134ec8aefab9a7510f2bb5ef7386caf',
               'mobile': mobile,
               'text': message.encode('utf-8'),
               }

        """
        {
            "code": 0,
            "msg": "OK",
            "result": {
                "count": 1,   //成功发送的短信个数
                "fee": 1,     //扣费条数，70个字一条，超出70个字时按每67字一条计
                "sid": 1097   //短信id；多个号码时以该id+各手机号尾号后8位作为短信id （数据类型：64位整型，对应Java和C#的long，不可用int解析)
            }
        }
        """

        try:
            tpl = urllib.urlencode(tpl)
            response = self.post(self.address, data=tpl).json()
        except RequestException:
            return {'status': 'fail', 'detail': 'request fail', 'retry': True}
        else:
            if response['code'] == 0:
                return {'status': 'success', 'msgid': response['result']['sid'], 'retry': False}
            else:
                return {'status': 'fail', 'detail': response['msg'], 'retry': False}

    def send_verify_code(self, mobile, verify_code):
        """ 发送验证码
        :return bool: 返回调用远程短信接口是否成功
        """

        msg = u'【付尔代网络科技】{0}（付尓代手机验证码，请完成验证），如非本人操作，请忽略本短信'.format(verify_code)
        return self.send_message(mobile, msg)

    def return_delivery_status(self):
        return 'SUCCESS'

    def parse_delivery_status(self, data):
        results = json.loads(urllib.unquote(data['sms_status']))

        if isinstance(results, dict):
            results = [results]

        ret = list()

        for data in results:
            gateway_message_id = str(data['sid'])
            state = str(data['report_status'])
            mobile = str(data['mobile'])

            try:
                delivery_date = datetime.strptime(str(data['user_receive_time']), '%Y-%m-%d %H:%M:%S')
            except ValueError:
                delivery_date = datetime.strptime(str(data['user_receive_time']), '%Y-%m-%d+%H:%M:%S')

            detail = state
            if state == 'SUCCESS':
                state = 'Delivered'
            else:
                state = 'Failed'

            ret.append({'gateway_message_id': gateway_message_id,
                        'state': state,
                        'mobile': mobile,
                        'delivery_date': delivery_date,
                        'detail': detail}
                       )

        return ret


if __name__ == '__main__':
    backend = YunPianBackend()
    print backend.send_verify_code(mobile='15067145712', verify_code='123cc456')
