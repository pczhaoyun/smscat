#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone
from datetime import timedelta, datetime
from django_extensions.db.fields import ShortUUIDField
from .settings import SEND_AS_SUCCESS


MESSAGE_STATUSES = (
    ('Unsent', u'未发送'),
    ('Sent', u'发送中'),
    ('Delivered', u'发送成功'),
    ('Failed', u'发送失败'),
)


class MessageManager(models.Manager):
    def get_unsent_msgs(self):
        """ 获取所有未发送的短信 """
        return self.filter(state='Unsent')

    def get_used_gateway(self, message_id):
        """ 获取当前消息ID已经使用的短信通道 """
        gateways = self.filter(message_id=message_id).values_list('gateway', flat=True)
        gateways = filter(lambda gateway: gateway, list(set(gateways)))
        return gateways

    def get_unhoped_msg(self):
        """  获取发送时间过长的短信 """
        time_threshold = datetime.now() - timedelta(minutes=1)
        return self.filter(send_date__lt=time_threshold, state='Sent')


# Create your models here.
class Message(models.Model):
    mobile = models.CharField(max_length=14, verbose_name=u"手机号", help_text=u"接收短信的手机号")
    message = models.CharField(max_length=1024, verbose_name=u"消息正文")
    state = models.CharField(verbose_name=u"消息状态",
                             max_length=16, choices=MESSAGE_STATUSES, default='Unsent', db_index=True)
    detail = models.CharField(verbose_name=u"原因", help_text=u"发送失败详细原因", max_length=512, default="")

    retry = models.IntegerField(verbose_name=u"重试次数", help_text=u"选择不同通道处理", default=0)
    send_date = models.DateTimeField(verbose_name=u"发送时间", help_text=u"创建时间", default=timezone.now)
    delivery_date = models.DateTimeField(null=True, blank=True, verbose_name=u"接收时间")

    gateway = models.CharField(max_length=32, blank=True, null=True, verbose_name=u"发送网关", help_text=u"发送网关")
    gateway_message_id = models.CharField(max_length=128, blank=True, null=True,
                                          verbose_name=u"外部消息ID", help_text=u"外部消息ID", db_index=True)
    message_id = ShortUUIDField(verbose_name=u"内部消息ID", db_index=True, primary_key=True)

    objects = MessageManager()

    class Meta:
        verbose_name = u"短信息"
        verbose_name_plural = u"短信息"
        ordering = ['-send_date']
        db_table = 'sms_message'

    def mark_failed(self):
        """ 标记当前消息发送失败 """
        self.state = 'Failed'
        self.save()

    def update_delivery_status(self, state, delivery_date, detail):
        """ 更新发送状态 """
        self.state = state
        self.delivery_date = delivery_date
        self.detail = detail
        self.save()

        if self.state == 'Failed':
            from .tasks import sms_handler

            new_msg = copy_message(self)
            sms_handler.delay(new_msg)

    def send_sms(self, gateway):
        """ use gateway to send message """
        max_retry = 3

        self.gateway = gateway.name

        while max_retry:
            response = gateway.send_message(self.mobile, self.message)

            if response['status'] == 'success':
                if SEND_AS_SUCCESS:
                    self.state = 'Delivered'
                else:
                    self.state = 'Sent'

                self.gateway_message_id = response['msgid']
                self.save()

                return True
            else:
                self.detail = response['detail'][:512]
                self.state = 'Failed'

                if not response['retry']:
                    self.save()
                    return False

            max_retry -= 1

        self.state = 'Failed'
        self.save()

        return False


def copy_message(msg):
    """ 复制一条新的消息 """

    msg.pk = None
    msg.gateway = None
    msg.gateway_message_id = None
    msg.delivery_date = None
    msg.detail = ''

    msg.state = 'Unsent'
    msg.retry += 1
    msg.send_date = timezone.now()

    msg.save()
    return msg
