#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from importlib import import_module
from backends import BACKENDS


def load_object(import_path):
    """
    Shamelessly stolen from https://github.com/ojii/django-load
    
    Loads an object from an 'import_path', like in MIDDLEWARE_CLASSES and the
    likes.

    Import paths should be: "mypackage.mymodule.MyObject". It then imports the
    module up until the last dot and tries to get the attribute after that dot
    from the imported module.

    If the import path does not contain any dots, a TypeError is raised.

    If the module cannot be imported, an ImportError is raised.

    If the attribute does not exist in the module, a AttributeError is raised.
    """
    if '.' not in import_path:
        raise TypeError(
            "'import_path' argument to 'load_object' must "
            "contain at least one dot."
        )
    module_name, object_name = import_path.rsplit('.', 1)
    module = import_module(module_name)
    return getattr(module, object_name)


def get_gateway(used=set(), name=""):
    """ 获取一个短信网关
    :param set used: 过滤已经使用的短信通道
    :param str name: 如果存在短信通道的名字，则直接获取短信通道
    :return: 返回短信通道的类或者None
    """

    if name:
        return BACKENDS[name]
    else:
        if not isinstance(used, set):
            used = set(used)

        all_gateways = set(BACKENDS.keys())
        valid = list(all_gateways - used)

        if len(valid):
            key = random.choice(valid)
            return BACKENDS[key]
        else:
            return None
