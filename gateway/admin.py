#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Message


class MessageAdmin(admin.ModelAdmin):
    model = Message
    search_fields = ('mobile', )
    list_filter = ('state', 'gateway', )
    list_display = ('mobile', 'message', 'detail', 'send_date', 'state', 'gateway', 'gateway_message_id', 'message_id')

admin.site.register(Message, MessageAdmin)
