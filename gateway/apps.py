#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.apps import AppConfig


class CustomAppConfig(AppConfig):
    name = 'gateway'
    verbose_name = u'短信网关'
