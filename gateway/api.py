#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from django.db import IntegrityError

from .models import Message
from .tasks import sms_handler


logger = logging.getLogger('sms-gateway')


def send_verify_code(mobile, verify_code):
    """ 向手机号发送短信
    :param str mobile: 手机号
    :param str verify_code: 验证码
    :return: 没有返回值，调用这个接口一定会成功
    """

    logger.debug('start send verify code %s to %s', verify_code, mobile)

    try:
        verify_code = u'{0}（付尓代手机验证码，请完成验证），如非本人操作，请忽略本短信'.format(verify_code)
        msg = Message.objects.create(mobile=mobile, message=verify_code)
    except IntegrityError:
        logger.error('send verify code %s to %s fail, create object fail', verify_code, mobile)

    sms_handler.delay(msg)

    return True


def send_message(mobile, message):
    """ 向手机号发送短信
    :param str mobile: 手机号
    :param str message: 需要发送的信息
    :return: 没有返回值，调用这个接口一定会成功
    """

    logger.debug('start send message to %s', mobile)

    try:
        msg = Message.objects.create(mobile=mobile, message=message)
    except IntegrityError:
        logger.error('send message to %s fail, create object fail', mobile)

    sms_handler.delay(msg)

    return True
