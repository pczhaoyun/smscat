# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('mobile', models.CharField(help_text='\u63a5\u6536\u77ed\u4fe1\u7684\u624b\u673a\u53f7', max_length=14, verbose_name='\u624b\u673a\u53f7')),
                ('message', models.CharField(max_length=1024, verbose_name='\u6d88\u606f\u6b63\u6587')),
                ('state', models.CharField(default=b'Unsent', max_length=16, verbose_name='\u6d88\u606f\u72b6\u6001', db_index=True, choices=[(b'Unsent', '\u672a\u53d1\u9001'), (b'Sent', '\u53d1\u9001\u4e2d'), (b'Delivered', '\u53d1\u9001\u6210\u529f'), (b'Failed', '\u53d1\u9001\u5931\u8d25')])),
                ('detail', models.CharField(default=b'', help_text='\u53d1\u9001\u5931\u8d25\u8be6\u7ec6\u539f\u56e0', max_length=512, verbose_name='\u539f\u56e0')),
                ('retry', models.IntegerField(default=0, help_text='\u9009\u62e9\u4e0d\u540c\u901a\u9053\u5904\u7406', verbose_name='\u91cd\u8bd5\u6b21\u6570')),
                ('send_date', models.DateTimeField(default=django.utils.timezone.now, help_text='\u521b\u5efa\u65f6\u95f4', verbose_name='\u53d1\u9001\u65f6\u95f4')),
                ('delivery_date', models.DateTimeField(null=True, verbose_name='\u63a5\u6536\u65f6\u95f4', blank=True)),
                ('gateway', models.CharField(help_text='\u53d1\u9001\u7f51\u5173', max_length=32, null=True, verbose_name='\u53d1\u9001\u7f51\u5173', blank=True)),
                ('gateway_message_id', models.CharField(max_length=128, blank=True, help_text='\u5916\u90e8\u6d88\u606fID', null=True, verbose_name='\u5916\u90e8\u6d88\u606fID', db_index=True)),
                ('message_id', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, blank=True, verbose_name='\u5185\u90e8\u6d88\u606fID', db_index=True)),
            ],
            options={
                'db_table': 'sms_message',
                'verbose_name': '\u77ed\u4fe1\u606f',
                'verbose_name_plural': '\u77ed\u4fe1\u606f',
            },
            bases=(models.Model,),
        ),
    ]
