# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gateway', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['-send_date'], 'verbose_name': '\u77ed\u4fe1\u606f', 'verbose_name_plural': '\u77ed\u4fe1\u606f'},
        ),
    ]
