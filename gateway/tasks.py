#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import

from celery import shared_task

from .models import Message, copy_message
from .utils import get_gateway


@shared_task
def sms_engine():
    """ 短信发送引擎，当一段时间内没有收到响应的时候直接切换通道 """

    messages = Message.objects.get_unhoped_msg()
    messages.update(state='Failed', detail=u"Sms Engine")

    for msg in messages:
        new_msg = copy_message(msg)
        sms_handler.s(new_msg)


@shared_task
def sms_handler(msg):
    """ 短信发送，
    发送失败立即切换通道，采用其他通道发送
    当在发送中状态的时候，如果长时间没有接收到通知，通过配置决定是否切换短信通道
    """

    used = Message.objects.get_used_gateway(msg.message_id)
    gateway = get_gateway(used=used)

    if gateway is not None:
        if msg.send_sms(gateway) is False:
            new_msg = copy_message(msg)
            sms_handler.s(new_msg)
        else:
            # 发送成功后，等待短信网关响应
            pass
    else:
        msg.mark_failed()


@shared_task
def sms_for_bind_handler(msg):
    """ 短信发送，
    发送失败立即切换通道，采用其他通道发送
    当在发送中状态的时候，如果长时间没有接收到通知，通过配置决定是否切换短信通道
    """
    gateway = get_gateway(name=msg.gateway)

    if gateway is not None:
        msg.send_sms(gateway)
    else:
        msg.mark_failed()
