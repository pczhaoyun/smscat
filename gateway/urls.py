#!/usr/bin/env python
# -*- coding: utf-8 -*-

import views
from django.conf.urls import patterns, url


urlpatterns = patterns('',
                       url('^send/$', views.Sender.as_view(), name='send-api'),
                       url('^sms/(?P<channel>\w+)/$', views.update_delivery_status, name='status_postback'),
                       )
