#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings


SEND_AS_SUCCESS = getattr(settings, 'SMS_SEND_AS_SUCCESS', True)  # 发送成功即标记成功
