#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.generics import CreateAPIView
from rest_framework import serializers

from .models import Message
from .utils import get_gateway
from .tasks import sms_handler, sms_for_bind_handler


logger = logging.getLogger('sms-gateway')


# Create your views here.
@csrf_exempt
def update_delivery_status(request, channel):
    gateway = get_gateway(name=channel)

    if request.method == 'GET':
        data = request.GET
    else:
        data = request.POST

    logger.debug(json.dumps(data))

    results = gateway.parse_delivery_status(data)

    for result in results:
        try:
            msg = Message.objects.get(gateway_message_id=result['gateway_message_id'])
        except Message.DoesNotExist:
            logger.info('not found message from gateway id %s, data = %s',
                        result['gateway_message_id'], json.dumps(data))
        else:
            # update delivery status
            msg.update_delivery_status(state=result['state'], delivery_date=result['delivery_date'],
                                       detail=result['detail'])

    return HttpResponse(gateway.return_delivery_status())


class MessageSerializer(serializers.ModelSerializer):
    mobile = serializers.CharField(label='mobile', help_text='mobile number', max_length=14, min_length=11)
    message = serializers.CharField(label='message', help_text='message content', max_length=512, min_length=1)
    bind = serializers.CharField(label='bind', help_text='use special channel', max_length=12, required=False)

    class Meta:
        model = Message
        fields = ('mobile', 'message', 'message_id', 'bind')

    def create(self, validated_data):
        if 'bind' in validated_data:
            instance = Message.objects.create(mobile=validated_data['mobile'],
                                              message=validated_data['message'],
                                              gateway=validated_data['bind']
                                              )
            sms_for_bind_handler.delay(instance)
        else:
            instance = Message.objects.create(mobile=validated_data['mobile'], message=validated_data['message'])
            sms_handler.delay(instance)

        return instance


class Sender(CreateAPIView):
    """ send sms api """
    model = Message
    serializer_class = MessageSerializer
